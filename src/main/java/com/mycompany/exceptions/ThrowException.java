/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exceptions;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author nunezd
 */
public class ThrowException {
    
    public static void main(String[] args) throws URISyntaxException {
        try {
            URI uri = new URI("http:\\somecompany.com");
        } catch (URISyntaxException e) {
                System.out.println(e.getMessage());
        }
        //this prints the exception
        
        System.out.println("I'm Alive");
    }
    
}
